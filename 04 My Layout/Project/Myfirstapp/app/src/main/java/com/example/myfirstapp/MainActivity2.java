package com.example.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

    }

    public void nextB2 (View view){
        Button clickB2 = findViewById(R.id.btnclick);
        Intent A2 = new Intent(MainActivity2.this,MainActivity.class);
        startActivity(A2);
    }


}